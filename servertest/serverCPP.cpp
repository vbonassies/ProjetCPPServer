// servertest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#undef UNICODE
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <string>
#include <thread>
#include <vector>
#include <fstream>
#include <ctime>
#pragma warning(disable : 4996) // Empeche l'erreur 4996 de pop 


#pragma comment (lib, "Ws2_32.lib")



#define DEFAULT_BUFLEN 512


struct client_type
{
	int id;
	SOCKET socket;
};

const char option_value = 1;
const int max_clients = 5;
int nb_message_user[max_clients];
std::string client_name[max_clients];
std::string nameUserSend;
std::string msgPrivate;

//Function Prototypes
int process_client(client_type &new_client, std::vector<client_type> &client_array, std::thread &thread);
int main();
std::string fileName;


int process_client(client_type &new_client, std::vector<client_type> &client_array, std::thread &thread)
{
	std::string msg;
	std::string ;
	char tempmsg[DEFAULT_BUFLEN] = "";


	//Session
	while (true)
	{
		memset(tempmsg, 0, DEFAULT_BUFLEN);


		if (new_client.socket != 0) //If no pb
		{
			auto i_result = recv(new_client.socket, tempmsg, DEFAULT_BUFLEN, 0);
			
			if (i_result != SOCKET_ERROR)
			{
				//Change user pseudo
				if (strcmp("", tempmsg) != 0)
				{
					if(nb_message_user[new_client.id] == 0)
					{
						client_name[new_client.id] = tempmsg;
						nb_message_user[new_client.id] = 1;
						std::cout << "Client #" << new_client.id << " is now known as " << client_name[new_client.id] <<std::endl;
						std::ofstream file(fileName, std::ios::app);
						file << "Client #" << new_client.id << " is now known as " << client_name[new_client.id] << std::endl;
					} else //User already have a pseudo, so save his msg
					{
						std::ofstream file(fileName, std::ios::app);
						msg = client_name[new_client.id] + ":" + tempmsg;
						file << msg << std::endl;
					}
				}
					

				std::cout << msg.c_str() << std::endl;

				//Broadcast that message to the other clients
				for (auto i = 0; i < max_clients; i++)
				{
					if (client_array[i].socket != INVALID_SOCKET)
						if (new_client.id != i)
							if (msg != "") {
								
								if (tempmsg[0] ==  '/') {

									int posSlash = msg.find_first_of("/");
									
									int posEspaceSecond = msg.find(" ", posSlash+1);
									std::string nameUserSend = msg.substr(posSlash+1, posEspaceSecond-posSlash-1);
									
									std::string msgPrivate = client_name[new_client.id] + " : " + msg.substr(posEspaceSecond + 1);
									
									if (nameUserSend == client_name[client_array[i].id]) {
										i_result = send(client_array[i].socket, msgPrivate.c_str(), strlen(msgPrivate.c_str()), 0);
									}

								}
								else {
									i_result = send(client_array[i].socket, msg.c_str(), strlen(msg.c_str()), 0);
								}
							}							
							
							
				}
			}
			else
			{
				msg = client_name[new_client.id] + " disconnected";
				client_name[new_client.id] = "";
				nb_message_user[new_client.id] = 0;

				std::ofstream file(fileName, std::ios::app);
				std::cout << msg << std::endl;
				file << msg << std::endl;

				closesocket(new_client.socket);
				closesocket(client_array[new_client.id].socket);
				client_array[new_client.id].socket = INVALID_SOCKET;

				//Broadcast the disconnection message to the other clients
				for (auto i = 0; i < max_clients; i++)
				{
					if (client_array[i].socket != INVALID_SOCKET)
						i_result = send(client_array[i].socket, msg.c_str(), strlen(msg.c_str()), 0);
				}

				break;
			}
		}
	}

	thread.detach();

	return 0;
}

int main()
{
	WSADATA wsa_data;
	struct addrinfo hints{};
	struct addrinfo *server = nullptr;
	auto server_socket = INVALID_SOCKET;
	std::string msg;
	std::vector<client_type> client(max_clients);
	std::string ip_add;
	std::string port;
	auto num_clients = 0;
	auto temp_id = -1;
	std::thread my_thread[max_clients];

	time_t t;
	time(&t);
	int min;
	int h;
	int day;
	int month;
	int year;
	struct tm *newT;
	newT = localtime(&t);
	h = newT->tm_hour;
	min = newT->tm_min;
	day = newT->tm_mday;
	month = newT->tm_mon + 1;
	year = newT->tm_year + 1900;


	fileName = "../logs_" + std::to_string(day) + "-" + std::to_string(month) + "-" + std::to_string(year);
	fileName += "_" + std::to_string(h) + "-" + std::to_string(min) + "_server.txt";


	//Initialize Winsock
	std::cout << "Intializing Winsock..." << std::endl;
	WSAStartup(MAKEWORD(2, 2), &wsa_data);

	//Setup hints
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	//Setup Server
	std::cout << "Please enter the address of the server, if you leave it blank it will go to default" << std::endl;
	getline(std::cin, ip_add);
	if (strcmp("", ip_add.c_str()) == 0)
	{
		std::cout << "default address chosen" << std::endl;
		ip_add = "127.0.0.1";
	}

	std::cout << "Please enter the port of the server, if you leave it blank it will go to default" << std::endl;
	getline(std::cin, port);
	if (strcmp("", port.c_str()) == 0)
	{
		std::cout << "default port chosen" << std::endl;
		port = "3504";
	}

	std::cout << "Setting up server..." << std::endl;
	getaddrinfo(ip_add.c_str(), port.c_str(), &hints, &server);

	//Create a listening socket for connecting to server
	std::cout << "Creating server socket..." << std::endl;
	server_socket = socket(server->ai_family, server->ai_socktype, server->ai_protocol);

	//Setup socket options
	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &option_value, sizeof(int)); //Make it possible to re-bind to a port that was used within the last 2 minutes
	setsockopt(server_socket, IPPROTO_TCP, TCP_NODELAY, &option_value, sizeof(int)); //Used for interactive programs

	//Assign an address to the server socket.
	std::cout << "Binding socket..." << std::endl;
	bind(server_socket, server->ai_addr, static_cast<int>(server->ai_addrlen));

	//Listen for incoming connections.
	std::cout << "Listening..." << std::endl;
	listen(server_socket, SOMAXCONN);

	//Initialize the client list
	for (auto i = 0; i < max_clients; i++)
	{
		client[i] = { -1, INVALID_SOCKET };
	}

	while (true)
	{
		auto incoming = INVALID_SOCKET;
		incoming = accept(server_socket, nullptr, nullptr);

		if (incoming == INVALID_SOCKET) continue;

		//Reset the number of clients
		num_clients = -1;

		//Create a temporary id for the next client
		temp_id = -1;
		for (auto i = 0; i < max_clients; i++)
		{
			if (client[i].socket == INVALID_SOCKET && temp_id == -1)
			{
				client[i].socket = incoming;
				client[i].id = i;
				temp_id = i;
			}

			if (client[i].socket != INVALID_SOCKET)
				num_clients++;
		}

		if (temp_id != -1)
		{
			//Send the id to that client
			std::ofstream file(fileName, std::ios::app);
			std::cout << "Client #" << client[temp_id].id << " Accepted" << std::endl;
			file << "Client #" << client[temp_id].id << " Accepted" << std::endl;
			msg = std::to_string(client[temp_id].id);
			send(client[temp_id].socket, msg.c_str(), strlen(msg.c_str()), 0);

			//Create a thread process for that client
			my_thread[temp_id] = std::thread(process_client, std::ref(client[temp_id]), std::ref(client), std::ref(my_thread[temp_id]));
		}
		else
		{
			msg = "Server is full";
			send(incoming, msg.c_str(), strlen(msg.c_str()), 0);
			std::cout << msg << std::endl;
			std::ofstream file(fileName, std::ios::app);
			file << msg << std::endl;
		}
	}
}


